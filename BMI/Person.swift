//
//  Person.swift
//  BMI
//
//  Created by iosdev on 3.4.2020.
//  Copyright © 2020 metropolia. All rights reserved.
//

import Foundation

class Person {
    
    //properties
    let name: String?
    var age: Int = 0
    var professions = Array<String>()
    var weight: Double
    var height: Double
    var BMI: Double
    //initialization
    init? (name: String, weight: Double, height: Double) {
        guard !name.isEmpty else {
            return nil
        }
        self.name = name
        self.weight = weight
        self.height = height
        self.BMI = self.weight/(self.height * self.height)
    }
    init? (_ name: String,_ age: Int,_ weight: Double,_ height: Double) {
        guard !name.isEmpty else {
            return nil
        }
        guard age > 0 && age<110 else {
            return nil
        }
        self.name = name
        self.age = age
        self.weight = weight
        self.height = height
        self.BMI = self.weight/(self.height * self.height)
    }
    //methods
    func addProfession(profession: String){
        if professions.count == 5 {
            professions.removeLast()
        }
        professions.append(profession)
    }
    func calcBMI() -> String {
        let square = ((self.height/100) * (self.height/100))
        let BMI = self.weight/square
        return String(BMI)
    }
    func aging() {
        self.age += 1
    }
    
    
    
    
    
}
