//
//  Storage.swift
//  BMI
//
//  Created by iosdev on 3.4.2020.
//  Copyright © 2020 metropolia. All rights reserved.
//

import Foundation

class Storage{
    static let shared = Storage()
    var bmis = [BMI]()
    private init(){}
    func getArray() -> Array<BMI> {
        return bmis
    }
    func add(bmi: BMI){
        bmis.append(bmi)
    }
    
}
