//
//  ViewController.swift
//  BMI
//
//  Created by iosdev on 2.4.2020.
//  Copyright © 2020 metropolia. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
var weightArray = Array<Int>()
var heightArray = Array<Int>()
    var selectedWeight = 0.0
    var selectedHeight = 0.0
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
        return weightArray.count
        } else {
            return heightArray.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return String(weightArray[row])
        } else {
            return String(heightArray[row])
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            selectedWeight = Double(weightArray[row])
            print(selectedWeight)
        } else {
            selectedHeight = Double(heightArray[row])
            print(selectedHeight)
        }
    }
    
    
    @IBOutlet weak var whPicker: UIPickerView!
    @IBOutlet weak var BMILabel: UILabel!
    @IBOutlet weak var nameField: UITextField!
    @IBAction func historyButton(_ sender: Any) {
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        whPicker.delegate = self as UIPickerViewDelegate
        whPicker.dataSource = self as UIPickerViewDataSource
        nameField.delegate = self as UITextFieldDelegate
        for i in 40..<151{
            weightArray.append(i)
        }
        for i in 140..<211{
            heightArray.append(i)
        }
        nameField.becomeFirstResponder()
        

}

//Action
    @IBAction func calculateButton(_ sender: UIButton) {
        var person = Person(name: nameField.text!, weight: selectedWeight, height: selectedHeight)
        BMILabel.text = person?.calcBMI()
        let bmi = BMI(height: selectedHeight, weight: selectedWeight)
        Storage.shared.add(bmi: bmi)
        
    }
    
//MARK; UITextFieldDelegate
       func textFieldDidBeginEditing(_ textField: UITextField) {
           print("started editing")
       }
       func textFieldDidChangeEditing(_ textField: UITextField) {
           print("changed")
       }
           func textFieldShouldReturn(_ textField: UITextField) -> Bool {
                   //hide keyboard
               textField.resignFirstResponder()
               return true
           }
           func textFieldDidEndEditing(_ textField: UITextField) {
               
           }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if range.location == 0 && string == " " { // prevent space on first character
            return false
        }

        if textField.text?.last == " " && string == " " { // allowed only single space
            return false
        }

        if string == " " { return true } // now allowing space between name

        if string.rangeOfCharacter(from: CharacterSet.letters.inverted) != nil {
            return false
        }

        return true
    }
    
        
       // Do any additional setup after loading the view.
    
   }


