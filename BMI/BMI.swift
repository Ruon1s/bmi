//
//  BMI.swift
//  BMI
//
//  Created by iosdev on 3.4.2020.
//  Copyright © 2020 metropolia. All rights reserved.
//

import Foundation
class BMI {
    let height: Double
    let weight: Double
    let BMI: Double
    
    init(height: Double, weight: Double){
        self.height = height
        self.weight = weight
        
        self.BMI = self.weight/((self.height/100)*(self.height/100))
    }
    
}
