//
//  PreviousCountCell.swift
//  BMI
//
//  Created by iosdev on 3.4.2020.
//  Copyright © 2020 metropolia. All rights reserved.
//

import UIKit

class PreviousCountCell: UITableViewCell {
    @IBOutlet weak var BMILabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
